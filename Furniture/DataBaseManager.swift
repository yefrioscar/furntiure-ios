//
//  DataBaseManager.swift
//  Furniture
//
//  Created by Yefri Laura on 12/7/17.
//  Copyright © 2017 Yefri Laura. All rights reserved.
//

import Foundation
import MagicalRecord

class DataBaseManager {
    
    static let shareInstance = DataBaseManager()
    
    func crearLibro() -> Producto {
        let a = Producto.mr_createEntity()
        
        a?.codigo = ""
        a?.nombre = ""
        a?.descuento = 0
        a?.precio = 0.0
        a?.imagen = ""
        a?.material = ""
        a?.tipo = ""
        a?.marca = ""
        a?.recomendaciones = ""
        a?.cantidad = 1
        a?.stock = 0
        
        
        
        self.SaveLibroDataBaseChanges()
        
        return a!
        
    }
    
    
    func deleteLibro(a: Producto) {
        a.mr_deleteEntity()
        self.SaveLibroDataBaseChanges()
    }
    
    func getLibroArray() -> NSArray {
        let arr: NSArray = Producto.mr_findAll()! as NSArray
        return arr
    }
    
    func getLibroByKey(attr: String, value: AnyObject) -> Producto {
        let a = Producto.mr_find(byAttribute: attr, withValue: value)
        
        return a![0] as! Producto
    }
    
    func deleteCompraTable(){
        Producto.mr_truncateAll()
        self.SaveLibroDataBaseChanges()
    }
    
    func SaveLibroDataBaseChanges() {
        NSManagedObjectContext.mr_default().mr_saveToPersistentStoreAndWait()
    }
    
    
    
    
}
