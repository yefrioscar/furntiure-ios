//
//  CarritoTableViewCell.swift
//  Furniture
//
//  Created by Yefri Laura on 12/10/17.
//  Copyright © 2017 Yefri Laura. All rights reserved.
//

import UIKit

protocol DailySpeakingLessonDelegate: class {
    func dailySpeakingLessonButtonPressed()
}

class CarritoTableViewCell: UITableViewCell {
    weak var delegate: DailySpeakingLessonDelegate?

    var producto: Producto!
    @IBOutlet weak var lblNombre: UILabel!
    @IBOutlet weak var lblPrecio: UILabel!
    @IBOutlet weak var lblDescuento: UILabel!
    
    @IBOutlet weak var image_producto: UIImageView!
    @IBOutlet weak var eliminarbtn: UIButton!
    
    
    @IBAction func eliminarProducto(_ sender: Any) {
        let can = DataBaseManager.shareInstance.getLibroByKey(attr: "codigo", value: (self.producto!.codigo as! AnyObject))
        
        DataBaseManager.shareInstance.deleteLibro(a: can)
    
        let notificationNme = NSNotification.Name("furnitureDelete")
        NotificationCenter.default.post(name: notificationNme, object: nil)
    }
    

    

    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func ini() {
        
        styleCell()
        
        let trash = UIImage(named: "garbage");
        self.eliminarbtn.setBackgroundImage(trash, for: UIControlState.normal)
        self.eliminarbtn.setTitle("", for: .normal)
        
        
        let link = self.producto!.imagen as! String
        self.image_producto.sd_setImage(with: URL(string: link))
        
        self.lblNombre.text = self.producto!.nombre
        
        let descuento1 =  self.producto!.descuento as! Int32

        let descuento2 =  Double(self.producto!.descuento)
        let precio =  Double(self.producto!.precio)
        
        let precioParse = precio - (precio * (descuento2 / 100))
        let d = String(format: "%.2f", precioParse);
        self.lblPrecio.text = "S/.\(d)"
        
        self.lblDescuento.text = "-\(descuento1)%"
    }
    
    func styleCell() {
        // Imagen
        self.image_producto.layer.cornerRadius = 5
        self.image_producto.layer.masksToBounds = true
        
        //labels
        let colorB = UIColor(red:0.03, green:0.17, blue:0.73, alpha:1.0)
        let colorS = UIColor(red:0.54, green:0.95, blue:0.59, alpha:1.0)
        
        self.lblDescuento.layer.cornerRadius = 3
        self.lblDescuento.layer.masksToBounds = true
        self.lblDescuento.backgroundColor = colorS
        self.lblDescuento.textColor = colorB
        self.lblDescuento.adjustsFontForContentSizeCategory = true
        self.lblDescuento.adjustsFontSizeToFitWidth = true
        self.lblDescuento.textAlignment = .center
        
        self.lblPrecio.layer.cornerRadius = 3
        self.lblPrecio.layer.masksToBounds = true
        self.lblPrecio.backgroundColor = colorB
        self.lblPrecio.textColor = colorS
        self.lblPrecio.adjustsFontSizeToFitWidth = true
        self.lblPrecio.adjustsFontForContentSizeCategory = true
        self.lblPrecio.textAlignment = .center
    }

}


