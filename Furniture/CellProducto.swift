//
//  CellProducto.swift
//  Furniture
//
//  Created by Yefri Laura on 12/4/17.
//  Copyright © 2017 Yefri Laura. All rights reserved.
//

import UIKit
import SDWebImage

class CellProducto: UICollectionViewCell {
    @IBOutlet weak var pro_imagen: UIImageView!
    @IBOutlet weak var pro_precio: UILabel!
    @IBOutlet weak var pro_descuento: UILabel!
    
    var producto: Dictionary<String,AnyObject>!
    
    
    
    func initCell() {
        styleCell()
        
        let img = self.producto["imagen"] as! String
        self.pro_imagen.sd_setImage(with: URL(string: img))
        
        let descuentotext = self.producto["descuento"] as! Int
        self.pro_descuento.text = "-\(descuentotext)%"
        
        let descuento = self.producto["descuento"] as! Double
        let precio = self.producto["precio"] as! Double
        let precioParse = precio - (precio * (descuento / 100))
        let d = String(format: "%.2f", precioParse);
        self.pro_precio.text = "S/.\(d)"

    }
    
    
    func styleCell() {
        
        // Imagen
        self.pro_imagen.layer.cornerRadius = 5
        self.pro_imagen.layer.masksToBounds = true
        
        //labels
        let colorB = UIColor(red:0.03, green:0.17, blue:0.73, alpha:1.0)
        let colorS = UIColor(red:0.54, green:0.95, blue:0.59, alpha:1.0)
        
        self.pro_descuento.layer.cornerRadius = 3
        self.pro_descuento.layer.masksToBounds = true
        self.pro_descuento.backgroundColor = colorS
        self.pro_descuento.textColor = colorB
        self.pro_precio.adjustsFontForContentSizeCategory = true
        self.pro_descuento.adjustsFontSizeToFitWidth = true
        self.pro_descuento.textAlignment = .center
        
        self.pro_precio.layer.cornerRadius = 3
        self.pro_precio.layer.masksToBounds = true
        self.pro_precio.backgroundColor = colorB
        self.pro_precio.textColor = colorS
        self.pro_precio.adjustsFontSizeToFitWidth = true
        self.pro_precio.adjustsFontForContentSizeCategory = true
        self.pro_precio.textAlignment = .center
    }
}
